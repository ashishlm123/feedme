package com.example.feedme.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.feedme.fragment.NearbyProvidersFrag;
import com.example.feedme.fragment.TodaysSpecialFrag;


/**
 * Created by Prabin on 8/3/2017.
 */

public class PageAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;

    public PageAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TodaysSpecialFrag tab1 = new TodaysSpecialFrag();
                return tab1;
            case 1:
                NearbyProvidersFrag tab2 = new NearbyProvidersFrag();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

