package com.example.feedme.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.feedme.R;
import com.example.feedme.model.CartModel;

import java.util.ArrayList;

public class CheckOutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CartModel> checkOutList;


    public CheckOutAdapter(Context context, ArrayList<CartModel> checkOutList) {
        this.checkOutList = checkOutList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_my_order_list_view, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        CartModel cartModel = checkOutList.get(position);
        viewHolder.itemName.setText(cartModel.getItemName());
        viewHolder.qty.setText(String.valueOf(cartModel.getQty()));
        viewHolder.totalPrice.setText("$ " + String.valueOf(cartModel.getTotalPrice()));

    }

    @Override
    public int getItemCount() {
        return checkOutList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView qty, itemName, totalPrice;

        ViewHolder(View itemView) {
            super(itemView);
            qty = itemView.findViewById(R.id.qty);
            itemName = itemView.findViewById(R.id.food_name);
            totalPrice = itemView.findViewById(R.id.food_price);
        }
    }
}
