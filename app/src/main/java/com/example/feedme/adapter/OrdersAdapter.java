package com.example.feedme.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.activity.CartActivity;
import com.example.feedme.activity.ItemDetailsActivity;
import com.example.feedme.activity.OrderInvoiceActivity;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.CartModel;
import com.example.feedme.model.ItemModel;
import com.example.feedme.model.OrderModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ashish on 25/07/2017.
 */
public class OrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<OrderModel> orderLists;
    PreferenceHelper helper;

    public OrdersAdapter(Context context, ArrayList<OrderModel> orderLists) {
        helper = PreferenceHelper.getInstance(context);
        this.orderLists = orderLists;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_ordered_food, parent, false));
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        OrderModel orderModel = orderLists.get(position);

        if (helper.getUserType().equals("1")) {
            viewHolder.layTvStatus.setVisibility(View.GONE);
            viewHolder.tvOrderFrom.setText("From: " + orderModel.getOrderProviderName());
            viewHolder.laySpStatus.setVisibility(View.VISIBLE);

            if (orderModel.getOrderStatus().equalsIgnoreCase(OrderModel.STATUS_PENDING)) {
                viewHolder.spStatus.setSelection(0);
                viewHolder.spStatus.setBackground(context.getDrawable(R.drawable.sp_pending_background));
            } else {
                viewHolder.spStatus.setSelection(1);
                viewHolder.spStatus.setBackground(context.getDrawable(R.drawable.sp_delivered_background));
            }
        } else {
            viewHolder.tvOrderFrom.setText("Ordered With: " + orderModel.getOrderProviderName());
            viewHolder.laySpStatus.setVisibility(View.GONE);
            viewHolder.layTvStatus.setVisibility(View.VISIBLE);
            if (orderModel.getOrderStatus().equalsIgnoreCase(OrderModel.STATUS_PENDING)) {
                viewHolder.tvStatus.setText(OrderModel.STATUS_PENDING);
                viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.c_red));
            } else {
                viewHolder.tvStatus.setText(OrderModel.STATUS_DELIVERED);
                viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.c_green));
            }
        }

        viewHolder.tvInvoiceId.setText("Invoice Id: " + orderModel.getOrderInvoiceId());
        viewHolder.totalPrice.setText("Total Price: " + "$ " + String.format("%.2f", orderModel.getOrderTotalPrice()));
        if (!TextUtils.isEmpty(orderModel.getOrderProviderImg())) {
            Picasso.with(context).load(orderModel.getOrderProviderImg()).into(viewHolder.profileIcon);
        }
    }


    @Override
    public int getItemCount() {
        return orderLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView totalPrice, tvInvoiceId, tvOrderFrom;
        ImageView profileIcon;
        Spinner spStatus;
        TextView tvStatus;

        LinearLayout laySpStatus, layTvStatus;

        ViewHolder(View itemView) {
            super(itemView);

            totalPrice = itemView.findViewById(R.id.tv_total_price);
            profileIcon = itemView.findViewById(R.id.iv_provider_img);
            tvStatus = itemView.findViewById(R.id.tv_status);
            spStatus = itemView.findViewById(R.id.sp_status);
            tvInvoiceId = itemView.findViewById(R.id.tv_invoice_id);
            tvOrderFrom = itemView.findViewById(R.id.tv_name);
            laySpStatus = itemView.findViewById(R.id.lay_sp_status);
            layTvStatus = itemView.findViewById(R.id.lay_tv_status);

            ArrayAdapter<String> userTypeArrayAdapter = new ArrayAdapter<>(context, R.layout.item_status_spinner,
                    context.getResources().getStringArray(R.array.item_status)); //selected item will look like a spinner set from XML
            userTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spStatus.setAdapter(userTypeArrayAdapter);

            spStatus.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 0) {
                                    if (NetworkUtils.isInNetwork(context)) {
                                        spStatus.setBackground(context.getDrawable(R.drawable.sp_pending_background));
                                        orderUpdateTask(orderLists.get(getLayoutPosition()), 0, view);
                                    } else {
                                        AlertUtils.showSnack(context, view, "No internet connection...");
                                    }

                                } else if (position == 1) {
                                    if (NetworkUtils.isInNetwork(context)) {
                                        spStatus.setBackground(context.getDrawable(R.drawable.sp_delivered_background));
                                        orderUpdateTask(orderLists.get(getLayoutPosition()), 1, view);
                                    } else {
                                        AlertUtils.showSnack(context, view, "No internet connection...");
                                    }
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                    return false;
                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (helper.getUserType().equals("1")) {
                        Intent intent = new Intent(context, OrderInvoiceActivity.class);
                        intent.putExtra("order", orderLists.get(getLayoutPosition()));
                        intent.putExtra("isProvider", true);
                        context.startActivity(intent);
                    } else {
                        if (orderLists.get(getLayoutPosition()).getOrderStatus().equalsIgnoreCase(OrderModel.STATUS_DELIVERED)) {
                            Intent intent = new Intent(context, OrderInvoiceActivity.class);
                            intent.putExtra("order", orderLists.get(getLayoutPosition()));
                            intent.putExtra("isProvider", false);
                            context.startActivity(intent);
                        } else {
                            AlertUtils.simpleAlert(context, "Delivery in Progress",
                                    "You will able to see invoice when the order gets delivered to you.",
                                    "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                        @Override
                                        public void onAlertButtonClick(boolean isPositiveButton) {

                                        }
                                    });
                        }

                    }
                }
            });
        }


        private void orderUpdateTask(final OrderModel orderModel, final int pos,
                                     final View view) {
            final ProgressDialog dialog = AlertUtils.showProgressDialog(context, "Please Wait...");

            VolleyHelper vHelper = VolleyHelper.getInstance(context);
            HashMap<String, String> postMap = new HashMap<>();
            postMap.put("order_id", orderModel.getOrderId());
            postMap.put("number", String.valueOf(pos));

            vHelper.addVolleyRequestListeners(Api.getInstance().updateOrderApi, Request.Method.POST, postMap,
                    new VolleyHelper.VolleyHelperInterface() {
                        @Override
                        public void onSuccess(String response) {
                            dialog.dismiss();
                            try {
                                JSONObject res = new JSONObject(response);
                                if (res.getBoolean("status")) {

                                } else {
                                    dialog.dismiss();
                                    AlertUtils.showSnack(context, view, res.getString("message"));
                                }

                            } catch (JSONException e) {
                                Logger.e("orderUpdateTask json ex", e.getMessage() + "");
                            }
                        }

                        @Override
                        public void onError(String errorResponse, VolleyError error) {
                            dialog.dismiss();
                            String errorMsg = "Unexpected Error, Please try again with internet access!";
                            try {
                                JSONObject errorObj = new JSONObject(errorResponse);
                                errorMsg = errorObj.getString("message");
                            } catch (JSONException e) {
                                Logger.e("orderUpdateTask error ex", e.getMessage() + " ");
                            }
                            AlertUtils.simpleAlert(context, "Error", errorMsg,
                                    "OK", "", null);
                        }
                    }, "orderUpdateTask");

        }

    }
}