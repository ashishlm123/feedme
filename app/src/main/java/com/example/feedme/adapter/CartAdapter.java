package com.example.feedme.adapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.activity.CartActivity;
import com.example.feedme.activity.ItemDetailsActivity;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.CartModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<CartModel> cartItemLists;

    public CartAdapter(Context context, ArrayList<CartModel> cartItemLists) {
        this.context = context;
        this.cartItemLists = cartItemLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false));

    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        CartModel cartModel = cartItemLists.get(position);

        viewHolder.tvItemName.setText(cartModel.getItemName());
        viewHolder.tvItemQty.setText(String.valueOf(cartModel.getQty()));
        viewHolder.tvItemPrice.setText("$ " + String.format("%.2f", cartModel.getItemPrice()));
        viewHolder.tvTotalPrice.setText("$ " + String.format("%.2f", cartModel.getTotalPrice()));
        if (!TextUtils.isEmpty(cartModel.getItemImage())) {
            Picasso.with(context).load(cartModel.getItemImage()).into(viewHolder.ivItemIcon);
        }
    }

    @Override
    public int getItemCount() {
        return cartItemLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName, tvItemQty, tvItemPrice, tvTotalPrice;
        ImageView ivItemIcon, ivItemDelete;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvItemName = itemView.findViewById(R.id.tv_item_name);
            tvItemQty = itemView.findViewById(R.id.qty);
            tvItemPrice = itemView.findViewById(R.id.tv_item_price);
            tvTotalPrice = itemView.findViewById(R.id.tv_totalPrice);

            ivItemIcon = itemView.findViewById(R.id.item_icon);
            ivItemIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ItemDetailsActivity.class);
                    intent.putExtra("item", cartItemLists.get(getLayoutPosition()));
                    intent.putExtra("isFromCart", true);
                    context.startActivity(intent);
                }
            });

            ivItemDelete = itemView.findViewById(R.id.delete_item);
            ivItemDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtils.isInNetwork(context)) {
                        userCartDeleteTask(cartItemLists.get((getLayoutPosition())), getLayoutPosition(), view);
                    } else {
                        AlertUtils.showSnack(context, view, "No internet connection...");
                    }
                }
            });

        }
    }

    private void userCartDeleteTask(final CartModel cartModel, final int pos, final View view) {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(context, "Please Wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("cart_id", cartModel.getCartId());

        vHelper.addVolleyRequestListeners(Api.getInstance().removeCartItemAPI, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                cartItemLists.remove(pos);
                                AlertUtils.showSnack(context, view, "Item deleted from cart");
                                notifyItemRemoved(pos);
                                ((CartActivity) context).updateWholePrice();

                                if (cartItemLists.size() == 0) {
                                    ((CartActivity) context).cartMessage();
                                }
                            } else {
                                dialog.dismiss();
                                AlertUtils.showSnack(context, view, res.getString("message"));
                            }

                        } catch (JSONException e) {
                            Logger.e("userCartDeleteTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("userCartDeleteTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(context, "Error", errorMsg,
                                "OK", "", null);
                    }
                }, "userCartDeleteTask");

    }

}
