package com.example.feedme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.feedme.R;
import com.example.feedme.model.UserModel;

import java.util.ArrayList;

public class DayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private String[] dayLists;
    ArrayList<Integer> selectedItems = new ArrayList<Integer>();

    public DayAdapter(Context context, String[] dayLists) {
        this.context = context;
        this.dayLists = dayLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_day, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        String day = dayLists[position];
        viewHolder.tvItemDay.setText(day);

        if (selectedItems.contains(position)) {
            viewHolder.tvItemDay.setBackgroundColor(context.getResources().getColor(R.color.c_green));
        } else {
            viewHolder.tvItemDay.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return dayLists.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemDay;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemDay = itemView.findViewById(R.id.tv_item_day);
            tvItemDay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedItems.contains(getLayoutPosition())) {
                        selectedItems.remove((Object) getLayoutPosition());
                        notifyDataSetChanged();
                    } else {
                        selectedItems.add(getLayoutPosition());
                        notifyDataSetChanged();
                    }
                }

            });
        }
    }

    public String getSelectedArray() {
        String allSelectedDays = "";
        for (int i = 0; i < selectedItems.size(); i++) {
            allSelectedDays = allSelectedDays + dayLists[selectedItems.get(i)] + ", ";
        }
        if (allSelectedDays.length() > 2)
            return allSelectedDays.substring(0, allSelectedDays.length() - 2);
        else
            return "";
    }
}
