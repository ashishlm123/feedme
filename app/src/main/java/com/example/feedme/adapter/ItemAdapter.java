package com.example.feedme.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.feedme.R;
import com.example.feedme.activity.CartActivity;
import com.example.feedme.activity.ItemDetailsActivity;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.model.ItemModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ashish on 25/07/2017.
 */
public class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<ItemModel> itemLists;
    PreferenceHelper helper;

    public ItemAdapter(Context context, ArrayList<ItemModel> itemLists) {
        helper = PreferenceHelper.getInstance(context);
        this.itemLists = itemLists;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_food, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        ItemModel itemModel = itemLists.get(position);
        viewHolder.foodName.setText(itemModel.getItemName());
        viewHolder.foodPrice.setText("$ " + String.format("%.2f", itemModel.getItemPrice()));
        if (!TextUtils.isEmpty(itemModel.getItemImage())) {
            Picasso.with(context).load(itemModel.getItemImage()).into(viewHolder.foodImage);
        }

    }


    @Override
    public int getItemCount() {
        return itemLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView foodPrice, foodName;
        ImageView foodImage;

        ViewHolder(View itemView) {
            super(itemView);

            foodPrice = itemView.findViewById(R.id.food_price);
            foodName = itemView.findViewById(R.id.food_name);
            foodImage = itemView.findViewById(R.id.food_icon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ItemDetailsActivity.class);
                    intent.putExtra("item", itemLists.get(getLayoutPosition()));
                    context.startActivity(intent);
                }
            });

        }
    }
}