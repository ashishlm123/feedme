package com.example.feedme.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.feedme.R;
import com.example.feedme.activity.ProviderProfileActivity;
import com.example.feedme.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProviderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<UserModel> userProviderLists;

    public ProviderAdapter(Context context, ArrayList<UserModel> userProviderLists) {
        this.context = context;
        this.userProviderLists = userProviderLists;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_provider, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        UserModel userModel = userProviderLists.get(position);

        viewHolder.tvUserName.setText(userModel.getUserName());
        viewHolder.tvUserEmail.setText(userModel.getUserEmail());
        if (!TextUtils.isEmpty(userModel.getUserImg())) {
            Picasso.with(context).load(userModel.getUserImg()).into(viewHolder.ivProviderIcon);
        }


    }

    @Override
    public int getItemCount() {
        return userProviderLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvUserEmail;
        ImageView ivProviderIcon;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.provider_name);
            tvUserEmail = itemView.findViewById(R.id.tv_provider_email);
            ivProviderIcon = itemView.findViewById(R.id.iv_provider_img);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProviderProfileActivity.class);
                    intent.putExtra("provider", userProviderLists.get(getLayoutPosition()));
                    intent.putExtra("isCustomer", true);
                    context.startActivity(intent);
                }
            });

        }
    }
}
