package com.example.feedme.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.adapter.ItemAdapter;
import com.example.feedme.adapter.ProviderAdapter;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.ItemModel;
import com.example.feedme.model.UserModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class NearbyProvidersFrag extends Fragment {

    private ArrayList<UserModel> nearbyProviderLists;
    private PreferenceHelper helper;
    private RecyclerView providerListing;
    private ProgressBar progressBar;
    private TextView tvErrorText;
    private ProviderAdapter providerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        providerListing = view.findViewById(R.id.list_recycler);
        providerListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        providerListing.setNestedScrollingEnabled(false);
        nearbyProviderLists = new ArrayList<>();

        providerAdapter = new ProviderAdapter(getActivity(), nearbyProviderLists);
        providerListing.setAdapter(providerAdapter);

        tvErrorText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);

        if (NetworkUtils.isInNetwork(getActivity())) {
            userNearbyProviderListingTask();
        } else {
            tvErrorText.setText("No Internet Connection...");
            tvErrorText.setVisibility(View.VISIBLE);
            providerListing.setVisibility(View.GONE);
        }
    }

    private void userNearbyProviderListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        providerListing.setVisibility(View.GONE);
        tvErrorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().getNearbyProvidersApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray providersArray = res.getJSONArray("provider_details");
                                String profileImgDirectory = res.getString("profile_image_directory");

                                if (providersArray.length() > 0) {
                                    for (int i = 0; i < providersArray.length(); i++) {
                                        JSONObject providerObject = providersArray.getJSONObject(i);
                                        UserModel userModel = new UserModel();
                                        userModel.setUserName(providerObject.getString("username"));
                                        userModel.setUserID(providerObject.getString("id"));
                                        userModel.setUserEmail(providerObject.getString("email"));
                                        userModel.setUserAddress(providerObject.getString("address"));
                                        userModel.setUserMobile(providerObject.getString("phoneno"));
                                        userModel.setUserType(providerObject.getString("usertype"));
                                        userModel.setUserAvailability(providerObject.getString("availability"));
                                        userModel.setUserFoodCertificateLink(providerObject.getString("foodcertificate"));

                                        String profileImg = providerObject.getString("user_image");
                                        if (!TextUtils.isEmpty(profileImg)) {
                                            userModel.setUserImg(profileImgDirectory + profileImg);
                                        }

                                        nearbyProviderLists.add(userModel);
                                    }

                                    providerAdapter.notifyDataSetChanged();

                                    providerListing.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                    Logger.e("Nearby Providers", "here");
                                } else {
                                    providerListing.setVisibility(View.GONE);
                                    tvErrorText.setText("No Providers Nearby");
                                    tvErrorText.setVisibility(View.VISIBLE);
                                }
                            } else {
                                tvErrorText.setVisibility(View.VISIBLE);
                                tvErrorText.setText(res.getString("message"));
                                providerListing.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userNearbyProviderListingTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("userNearbyProviderListingTask error ex", e.getMessage() + " ");
                        }

                    }
                }, "userNearbyProviderListingTask");

    }
}
