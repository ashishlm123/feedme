package com.example.feedme.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.adapter.ItemAdapter;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.ItemModel;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class TodaysSpecialFrag extends Fragment {
    private ArrayList<ItemModel> itemLists;
    private PreferenceHelper helper;
    private RecyclerView todaySpecialListing;
    private ProgressBar progressBar;
    private TextView tvErrorText;
    private ItemAdapter todaySpecialAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        todaySpecialListing = view.findViewById(R.id.list_recycler);
        todaySpecialListing.setLayoutManager(new LinearLayoutManager(getActivity()));
        todaySpecialListing.setNestedScrollingEnabled(false);
        itemLists = new ArrayList<>();


        todaySpecialAdapter = new ItemAdapter(getActivity(), itemLists);
        todaySpecialListing.setAdapter(todaySpecialAdapter);

        tvErrorText = view.findViewById(R.id.error_text);
        progressBar = view.findViewById(R.id.progress_bar);

        if (NetworkUtils.isInNetwork(getActivity())) {
            userTodaysSpecialListingTask();
        } else {
            tvErrorText.setText("No Internet Connection...");
            tvErrorText.setVisibility(View.VISIBLE);
            todaySpecialListing.setVisibility(View.GONE);
        }

    }

    private void userTodaysSpecialListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        todaySpecialListing.setVisibility(View.GONE);
        tvErrorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().todaysSpecialApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray todaysSpecialItemsArray = res.getJSONArray("todays_special_details");
                                String profileImgDirectory = res.getString("profile_image_directory");

                                if (todaysSpecialItemsArray.length() > 0) {
                                    for (int i = 0; i < todaysSpecialItemsArray.length(); i++) {
                                        JSONObject itemObject = todaysSpecialItemsArray.getJSONObject(i);
                                        ItemModel itemModel = new ItemModel();
                                        itemModel.setItemId(itemObject.getString("item_id"));
                                        itemModel.setItemName(itemObject.getString("item_name"));
                                        itemModel.setItemPrice(Double.parseDouble(itemObject.getString("item_price")));

                                        String itemImg = itemObject.getString("imagepath");
                                        if (!TextUtils.isEmpty(itemImg)) {
                                            itemModel.setItemImage(profileImgDirectory + itemImg);
                                        }

                                        //since this is today's special api, item coming from this api will always be the item that is inside today's special
                                        itemModel.setTodaysSpecial(true);
                                        itemLists.add(itemModel);
                                    }

                                    todaySpecialAdapter.notifyDataSetChanged();

                                    todaySpecialListing.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                    Logger.e("Todays Special Fragment", "here");
                                } else {
                                    todaySpecialListing.setVisibility(View.GONE);
                                    tvErrorText.setText("No Special items to show");
                                    tvErrorText.setVisibility(View.VISIBLE);
                                }
                            } else {
                                tvErrorText.setVisibility(View.VISIBLE);
                                tvErrorText.setText(res.getString("message"));
                                todaySpecialListing.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userTodaysSpecialListingTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("userTodaysSpecialListingTask error ex", e.getMessage() + " ");
                        }

                    }
                }, "userItemListingTask");

    }
}
