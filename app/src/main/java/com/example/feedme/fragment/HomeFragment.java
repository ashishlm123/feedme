package com.example.feedme.fragment;

import android.os.Bundle;
import android.os.Handler;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import com.example.feedme.R;
import com.example.feedme.adapter.PageAdapter;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.PreferenceHelper;
import com.google.android.material.tabs.TabLayout;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;


public class HomeFragment extends Fragment {
    private ArrayList<Integer> sliderImage = new ArrayList<>(Arrays.asList(R.drawable.slider1, R.drawable.slider2, R.drawable.slider3, R.drawable.slider4));
    private PreferenceHelper helper;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frament_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        helper = PreferenceHelper.getInstance(getActivity());

        setBannerImage(view, sliderImage);

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Today's Special"));
        tabLayout.addTab(tabLayout.newTab().setText("Nearby Providers"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = view.findViewById(R.id.pager);
        PageAdapter pageAdapter = new PageAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    private void setBannerImage(View view, final ArrayList<Integer> images) {
        final SliderLayout imageSlider = view.findViewById(R.id.slider);
        final PagerIndicator pagerIndicator = view.findViewById(R.id.custom_indicator);

        imageSlider.stopAutoCycle();
        imageSlider.setVisibility(View.GONE);
        for (Integer name : images) {
            DefaultSliderView defaultSlider = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSlider
                    .image(name)
                    .setScaleType(BaseSliderView.ScaleType.Fit);

            //add your extra information
            defaultSlider.bundle(new Bundle());
            defaultSlider.getBundle()
                    .putString("extra", String.valueOf(name));

            imageSlider.addSlider(defaultSlider);

            try {
                Class<?> c = pagerIndicator.getClass();
                Field f = c.getDeclaredField("mPreviousSelectedPosition");
                f.setAccessible(true);
                f.setInt(pagerIndicator, -1);
                f.setAccessible(false);
            } catch (Exception e) {
                Logger.e("reflection ex", e.getMessage() + " ");
            }
        }

        if (images.size() > 1) {
            imageSlider.setCurrentPosition(0); //workaround for auto slide to second
            imageSlider.setDuration(40000);
            imageSlider.setCustomAnimation(new DescriptionAnimation());
            imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);

            try {
                Class<?> c = pagerIndicator.getClass();
                Field f = c.getDeclaredField("mPreviousSelectedPosition");
                f.setAccessible(true);
                f.setInt(pagerIndicator, -1);
                f.setAccessible(false);
            } catch (Exception e) {
                Logger.e("reflection ex", e.getMessage() + " ");
            }
            imageSlider.setCustomIndicator(pagerIndicator);
            imageSlider.setCurrentPosition(0, true);
            // play from first image, when all images loaded
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        Class<?> c = pagerIndicator.getClass();
                        Field f = c.getDeclaredField("mPreviousSelectedPosition");
                        f.setAccessible(true);
                        f.setInt(pagerIndicator, -1);
                        f.setAccessible(false);

                        imageSlider.setCustomIndicator(pagerIndicator);
                        pagerIndicator.onPageSelected(0);
                        imageSlider.setCurrentPosition(0);
                        imageSlider.setDuration(4000);
                        imageSlider.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imageSlider.startAutoCycle();
                                } catch (Exception e) {
                                }
                            }
                        }, 4400);
                    } catch (Exception e) {
                        Logger.e("reflection ex", e.getMessage() + " ");
                    }
                }
            }, 900);

        } else {
            imageSlider.setVisibility(View.VISIBLE);
            pagerIndicator.setVisibility(View.GONE);
            imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            imageSlider.stopAutoCycle();
        }
    }
}
