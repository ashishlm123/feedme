package com.example.feedme.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;

import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ContactUsFrag extends Fragment {
    private PreferenceHelper prefsHelper;
    private EditText etEmail, etContact, etMsg;
    private TextView tvErrEmail, tvErrNumber, tvErrMessage;
    private Button btnContactUS;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(getActivity());

        etEmail = view.findViewById(R.id.edt_email);
        etEmail.setText(prefsHelper.getUserEmail());
        etContact = view.findViewById(R.id.edt_contact_number);
        etContact.setText(prefsHelper.getAppUserMobileNo());
        etMsg = view.findViewById(R.id.edt_enter_message);

        tvErrEmail = view.findViewById(R.id.err_email);
        tvErrNumber = view.findViewById(R.id.tv_err_contact);
        tvErrMessage = view.findViewById(R.id.tv_err_message);

        btnContactUS = view.findViewById(R.id.btn_contact_us);
        btnContactUS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    if (validation()) {
                        if (NetworkUtils.isInNetwork(getActivity())) {
                            userContactUsTask(etEmail.getText().toString().trim(),
                                    etMsg.getText().toString().trim(),
                                    etContact.getText().toString().trim());
                        } else {
                            AlertUtils.showSnack(getActivity(), view, "Try again with Internet Access.");
                        }
                    }
                }
            }
        });
    }

    private void userContactUsTask(final String email, String message, String contact) {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(getActivity(), "Sending Message...");

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("email", email);
        postParams.put("phoneno", contact);
        postParams.put("message", message);

        vHelper.addVolleyRequestListeners(Api.getInstance().contactUsApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.simpleAlert(getActivity(), "Success", res.getString("message"), "OK", "Cancel", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                etContact.setText("");
                                etEmail.setText("");
                                etMsg.setText("");
                            }
                        });
                    } else {
                        AlertUtils.showSnack(getActivity(), etContact, res.getString("message"));
                    }

                } catch (JSONException e) {
                    Logger.e("userContactUsTask json ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected Error, Please try again with internet access!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("userContactUsTask error ex", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(getActivity(), "Error", errorMsg,
                        "OK", "", null);
            }
        }, "userContactUsTask");

    }

    public boolean validation() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            isValid = false;
            tvErrEmail.setVisibility(View.VISIBLE);
            tvErrEmail.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                tvErrEmail.setVisibility(View.GONE);
            } else {
                Toast.makeText(getActivity(), "Invalid Email Address", Toast.LENGTH_SHORT).show();
            }
        }

        if (TextUtils.isEmpty(etContact.getText().toString().trim())) {
            isValid = false;
            tvErrNumber.setVisibility(View.VISIBLE);
            tvErrNumber.setText("This field is required...");
        } else {
            tvErrNumber.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(etMsg.getText().toString().trim())) {
            isValid = false;
            tvErrMessage.setVisibility(View.VISIBLE);
            tvErrMessage.setText("This field is required...");
        } else {
            tvErrMessage.setVisibility(View.GONE);
        }

        return isValid;
    }
}
