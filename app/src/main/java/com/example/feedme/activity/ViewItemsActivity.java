package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.adapter.ItemAdapter;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.ItemModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ViewItemsActivity extends AppCompatActivity {
    ArrayList<ItemModel> itemLists;
    PreferenceHelper helper;
    RecyclerView itemListings;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView tvErrorText;
    ItemAdapter itemAdapter;
    Button btnAddItems;
    String providerId = "";
    boolean isCustomer = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_items);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Available Items");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        isCustomer = intent.getBooleanExtra("isCustomer", false);
        if (isCustomer) {
            providerId = intent.getStringExtra("provider_id");
            Logger.e("providerId", providerId);
        }

        helper = PreferenceHelper.getInstance(this);
        btnAddItems = findViewById(R.id.btn_addItems);

        if (helper.getUserType().equals("1")) {
            btnAddItems.setVisibility(View.VISIBLE);
            btnAddItems.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddItemsActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            btnAddItems.setVisibility(View.GONE);
        }

        itemListings = findViewById(R.id.list_recycler);
        itemListings.setLayoutManager(new LinearLayoutManager(ViewItemsActivity.this));
        itemListings.setNestedScrollingEnabled(false);
        itemLists = new ArrayList<>();
        itemAdapter = new ItemAdapter(ViewItemsActivity.this, itemLists);
        itemListings.setAdapter(itemAdapter);

        tvErrorText = findViewById(R.id.error_text);
        progressBar = findViewById(R.id.progress_bar);

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            userItemListingTask();
        } else {
            tvErrorText.setText("No Internet Connection...");
            tvErrorText.setVisibility(View.VISIBLE);
            itemListings.setVisibility(View.GONE);
        }

    }

    private void userItemListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        itemListings.setVisibility(View.GONE);
        tvErrorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(ViewItemsActivity.this);
        HashMap<String, String> postParams;
        if (isCustomer) {
            postParams = new HashMap<>();
            if (PreferenceHelper.getInstance(ViewItemsActivity.this).isLogin()) {
                postParams.put("id", providerId);
            }
        } else {
            postParams = vHelper.getPostParams();
        }


        vHelper.addVolleyRequestListeners(Api.getInstance().viewAvailableItemsApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray availableItemsArray = res.getJSONArray("item_details");
                                String profileImgDirectory = res.getString("profile_image_directory");

                                if (availableItemsArray.length() > 0) {
                                    for (int i = 0; i < availableItemsArray.length(); i++) {
                                        JSONObject itemObject = availableItemsArray.getJSONObject(i);
                                        ItemModel itemModel = new ItemModel();
                                        itemModel.setItemId(itemObject.getString("item_id"));
                                        itemModel.setItemName(itemObject.getString("item_name"));
                                        itemModel.setItemPrice(Double.parseDouble(itemObject.getString("item_price")));

                                        String itemImg = itemObject.getString("imagepath");
                                        if (!TextUtils.isEmpty(itemImg)) {
                                            itemModel.setItemImage(profileImgDirectory + itemImg);
                                        }

                                        if (itemObject.getString("todays_special").equals("1")) {
                                            itemModel.setTodaysSpecial(true);
                                        } else {
                                            itemModel.setTodaysSpecial(false);
                                        }
                                        itemLists.add(itemModel);
                                    }

                                    itemAdapter.notifyDataSetChanged();

                                    itemListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                    Logger.e("View Items Activity", "here");
                                } else {
                                    itemListings.setVisibility(View.GONE);
                                    tvErrorText.setText("No Items Available at the moment");
                                    tvErrorText.setVisibility(View.VISIBLE);
                                }
                            } else {
                                tvErrorText.setVisibility(View.VISIBLE);
                                tvErrorText.setText(res.getString("message"));
                                itemListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userItemListingTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("userItemListingTask error ex", e.getMessage() + " ");
                        }

                    }
                }, "userItemListingTask");
    }

}
