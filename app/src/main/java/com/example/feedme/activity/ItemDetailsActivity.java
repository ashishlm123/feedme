package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.CartModel;
import com.example.feedme.model.ItemModel;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ItemDetailsActivity extends AppCompatActivity {
    ImageView itemIcon, ivLocation;
    TextView tvItemName, tvItemPrice, tvContact, tvProviderName, tvMinus, tvPlus, qty, tvTotalPrice, tvTodaysSpecial;
    Button btnAddToCart, btnCheckOut, btnDeleteItem;
    Toolbar toolbar;
    ItemModel itemModel;
    PreferenceHelper helper;
    private int REQUEST_PHONE_CALL = 101;
    CardView layCardView, layItemDetails;
    LinearLayout layButtonView;
    String phnNo = "";
    String providerId = "";
    CartModel cartModel;
    boolean isFromCart = false;

    String itemId = "";

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        helper = PreferenceHelper.getInstance(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final Intent intent = getIntent();
        itemModel = (ItemModel) intent.getSerializableExtra("item");

        final Intent intent1 = getIntent();
        isFromCart = intent1.getBooleanExtra("isFromCart", false);
        if (isFromCart) {
            cartModel = (CartModel) intent1.getSerializableExtra("item");
            assert cartModel != null;
            itemId = cartModel.getItemId();
            Logger.e("iscaritemid", itemId);
        }

        toolbar.setTitle("Item Details");

        layCardView = findViewById(R.id.lay_card_view);
        layButtonView = findViewById(R.id.lay_button_view);

        layItemDetails = findViewById(R.id.lay_item_Details);

        itemIcon = findViewById(R.id.item_icon);
        if (!TextUtils.isEmpty(itemModel.getItemImage())) {
            Picasso.with(ItemDetailsActivity.this).load(itemModel.getItemImage()).into(itemIcon);
        }

        tvItemName = findViewById(R.id.tv_item_name);
        tvTotalPrice = findViewById(R.id.tv_totalPrice);
        tvItemPrice = findViewById(R.id.tv_item_price);
        tvTodaysSpecial = findViewById(R.id.tv_todays_special);
        qty = findViewById(R.id.qty);

        if (isFromCart) {
            tvItemName.setText(cartModel.getItemName());
            tvItemPrice.setText("$ " + String.format("%.2f", cartModel.getItemPrice()));
            tvTotalPrice.setText("$ " + String.format("%.2f", cartModel.getTotalPrice()));

            if (cartModel.isTodaysSpecial()) {
                tvTodaysSpecial.setText("Yes");
            } else {
                tvTodaysSpecial.setText("No");
            }
            qty.setText(String.valueOf(cartModel.getQty()));
            Logger.e("here", "here");
        } else {
            tvItemName.setText(itemModel.getItemName());
            tvItemPrice.setText("$ " + String.format("%.2f", itemModel.getItemPrice()));
            tvTotalPrice.setText("$ " + String.format("%.2f", itemModel.getItemPrice()));
            if (itemModel.isTodaysSpecial()) {
                tvTodaysSpecial.setText("Yes");
            } else {
                tvTodaysSpecial.setText("No");
            }

            itemId = itemModel.getItemId();
            Logger.e("isItemId", itemId);
        }

        tvMinus = findViewById(R.id.tv_minus);
        tvPlus = findViewById(R.id.tv_plus);

        btnAddToCart = findViewById(R.id.btn_add_to_cart);
        btnCheckOut = findViewById(R.id.btn_checkOut);
        btnDeleteItem = findViewById(R.id.btn_delete_item);


        tvMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int a = Integer.parseInt(String.valueOf(qty.getText()));
                if (a > 1) {
                    a--;
                } else {
                    a = 1;
                }
                qty.setText(String.valueOf(a));
                double totalPrice = itemModel.getItemPrice() * a;
                tvTotalPrice.setText("$" + String.format("%.2f", totalPrice));
            }
        });


        tvPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer a = Integer.parseInt(String.valueOf(qty.getText()));
                a++;
                qty.setText(String.valueOf(a));
                double totalPrice = itemModel.getItemPrice() * a;
                tvTotalPrice.setText("$" + String.format("%.2f", totalPrice));
            }
        });

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    Log.e("Cart ma Aaipugyo?", "Ah, Balla Ball Aayipugyo Ghumera");
                    userAddToCartTask();
                } else {
                    AlertUtils.showSnack(ItemDetailsActivity.this, view, "No Internet Connection...");
                }
            }
        });

        btnDeleteItem = findViewById(R.id.btn_delete_item);
        btnDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    userDeleteItemTask();
                }
            }
        });

        if (helper.getUserType().equals("1")) {
            layButtonView.setVisibility(View.GONE);
            layCardView.setVisibility(View.GONE);
            btnDeleteItem.setVisibility(View.VISIBLE);
        } else {
            layCardView.setVisibility(View.VISIBLE);
            layButtonView.setVisibility(View.VISIBLE);
            btnDeleteItem.setVisibility(View.GONE);
        }

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            userItemDetailsTask();
        } else {
            AlertUtils.showToast(ItemDetailsActivity.this, "Please, check your internet connection..");
        }

        tvContact = findViewById(R.id.tv_provider_contact);
        tvContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setAction("android.intent.action.DIAL");
                callIntent.setData(Uri.parse("tel:" + phnNo));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ItemDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ItemDetailsActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                    } else {
                        startActivity(callIntent);
                    }
                }
            }
        });

        tvProviderName = findViewById(R.id.tv_provider_name);

    }

    private void userDeleteItemTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(ItemDetailsActivity.this, "Please Wait...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        postParams.put("item_id", itemModel.getItemId());

        vHelper.addVolleyRequestListeners(Api.getInstance().deleteItemApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                dialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.simpleAlert(ItemDetailsActivity.this, false, "Success",
                                "Item Deleted Sucessfully.", "Ok", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        Intent intent = new Intent(getApplicationContext(), ProviderProfileActivity.class);
                                        startActivity(intent);
                                    }
                                });
                    } else {
                        AlertUtils.simpleAlert(ItemDetailsActivity.this, false, "Error",
                                res.getString("message"), "Ok", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        Intent intent = new Intent(getApplicationContext(), ProviderProfileActivity.class);
                                        startActivity(intent);
                                    }
                                });
                    }
                } catch (JSONException e) {
                    Logger.e("userAddToCartTask json ex", e.getMessage() + "");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                dialog.dismiss();
            }
        }, "userDeleteItemTask");
    }

    private void userAddToCartTask() {
        final ProgressDialog dialog = AlertUtils.showProgressDialog(ItemDetailsActivity.this, "Please Wait...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("provider_id", providerId);
        postMap.put("item_id", itemId);
        postMap.put("item_quantity", qty.getText().toString().trim());
        postMap.put("item_price", String.valueOf(itemModel.getItemPrice()));


        vHelper.addVolleyRequestListeners(Api.getInstance().addToCarApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        dialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                Snackbar mSnackbar = Snackbar.make(btnAddToCart, res.getString("message"),
                                        Snackbar.LENGTH_INDEFINITE).setAction("View Cart", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                                        intent.putExtra("provider_id", providerId);
                                        Logger.e("providerId", providerId);
                                        startActivity(intent);
                                    }
                                });
                                dialog.dismiss();
                                mSnackbar.show();
                            } else {
                                AlertUtils.simpleAlert(ItemDetailsActivity.this, false, "Error", res.getString("message"), "OK", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                                        startActivity(intent);
                                    }
                                });

                                dialog.dismiss();

//                                AlertUtils.showSnack(FoodDetailsActivity.this, foodName, Html.fromHtml(res.getString("message")).toString());
                            }
                        } catch (JSONException e) {
                            Logger.e("userAddToCartTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        dialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(ItemDetailsActivity.this, tvItemName, errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                Toast.makeText(getApplicationContext(), "No Internet Connection...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ServerError) {
                                Toast.makeText(getApplicationContext(), "Server Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof AuthFailureError) {
                                Toast.makeText(getApplicationContext(), "Authentication Error...", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof ParseError) {
                                Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                            } else if (error instanceof TimeoutError) {
                                Toast.makeText(getApplicationContext(), "Timed Out", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, "userAddToCartTask");
    }

    private void userItemDetailsTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(ItemDetailsActivity.this, "Please Wait...");

        layItemDetails.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();
        postMap.put("item_id", itemModel.getItemId());

        vHelper.addVolleyRequestListeners(Api.getInstance().itemDetailsApi, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        progressDialog.dismiss();
                        JSONObject itemObject = res.getJSONObject("item_detail");
                        tvContact.setText(itemObject.getString("phoneno"));
                        phnNo = itemObject.getString("phoneno");

                        //take this provider id wherever you need.
                        providerId = itemObject.getString("id");

                        Logger.e("providerID", providerId);
                        tvProviderName.setText(itemObject.getString("username"));
                        layItemDetails.setVisibility(View.VISIBLE);
                    } else {
                        progressDialog.dismiss();
                        AlertUtils.showSnack(ItemDetailsActivity.this, tvItemName, "Unknown Issue");
                    }
                } catch (JSONException e) {
                    Logger.e("userItemDetailsTask error ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errorMsg = "Unexpected Error, Please try again with internet access!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("userItemDetailsTask error ex", e.getMessage() + " ");
                }

            }
        }, "userItemDetailsTask");


    }


}
