package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;


import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.UserModel;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class ProviderProfileActivity extends AppCompatActivity {
    Toolbar toolbar;
    ImageView ivProviderMapAddress;
    TextView tvProviderName, tvProviderAddress, tvAvailability, tvAvailableItems, tvContact, tvOrders;
    UserModel userModel;
    PreferenceHelper helper;
    boolean isProviderLoggedIn;
    ImageView ivProviderImg;
    boolean isCustomer = false;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_details);
        helper = PreferenceHelper.getInstance(this);
        ivProviderImg = findViewById(R.id.iv_provider_img);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Provider Details");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvProviderName = findViewById(R.id.tv_provider_name);
        tvProviderAddress = findViewById(R.id.tv_provider_address);
        tvContact = findViewById(R.id.tv_provider_contact);
        tvAvailability = findViewById(R.id.tv_provider_availability);
        tvAvailableItems = findViewById(R.id.tv_available_menu);
        tvOrders = findViewById(R.id.tv_orders);

        final Intent intent = getIntent();

        if (helper.getUserType().equals("1")) {
            toolbar.setNavigationIcon(null);
            Logger.e("isCustomer", String.valueOf(isCustomer));

            toolbar.inflateMenu(R.menu.sign_out);
            Menu menu = toolbar.getMenu();
            MenuItem signOut = menu.findItem(R.id.sign_out);

            signOut.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    if (item.getItemId() == R.id.sign_out) {
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        helper.edit().clear().apply();
                        startActivity(intent);
                    }
                    return false;
                }
            });

            tvProviderName.setText("Name: " + helper.getAppUserFullName());
            tvAvailability.setText("Availability: " + helper.getUserAvailability());
            tvContact.setText("Contact: " + helper.getAppUserMobileNo());
            tvProviderAddress.setText("Address: " + helper.getUserAddress());
            if (!TextUtils.isEmpty(helper.getAppUserImg())) {
                Picasso.with(ProviderProfileActivity.this).load(helper.getAppUserImg()).into(ivProviderImg);
            }

            tvOrders.setVisibility(View.VISIBLE);
        } else {
            userModel = (UserModel) intent.getSerializableExtra("provider");
            isCustomer = intent.getBooleanExtra("isCustomer", false);
            Logger.e("isCustomer", String.valueOf(isCustomer));

            tvProviderName.setText("Name: " + userModel.getUserName());
            tvProviderAddress.setText("Address: " + userModel.getUserAddress());
            tvContact.setText("Contact: " + userModel.getUserMobile());
            if (!TextUtils.isEmpty(helper.getAppUserImg())) {
                Picasso.with(ProviderProfileActivity.this).load(userModel.getUserImg()).into(ivProviderImg);
            }
            tvOrders.setVisibility(View.GONE);
        }

        tvAvailableItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ViewItemsActivity.class);
                if (isCustomer) {
                    intent.putExtra("provider_id", userModel.getUserID());
                    intent.putExtra("isCustomer", true);
                    startActivity(intent);
                } else {
                    startActivity(intent);
                }
            }
        });

        tvOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), OrdersActivity.class);
                startActivity(intent);
            }
        });
    }

}

