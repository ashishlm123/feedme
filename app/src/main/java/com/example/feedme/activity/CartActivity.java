package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.adapter.CartAdapter;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.CartModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CartActivity extends AppCompatActivity {
    CartAdapter cartAdapter;
    ArrayList<CartModel> cartItemLists;
    RecyclerView rvCartItemListings;
    Toolbar toolbar;
    Button btnAddItem, btnCheckOut;
    TextView tvItemName, tvItemPrice, tvItemTotalPrice, tvWholeTotalPrice;
    PreferenceHelper helper;
    ProgressBar progressBar;
    TextView tvErrorText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        helper = PreferenceHelper.getInstance(this);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("My Cart");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        rvCartItemListings = findViewById(R.id.rv_item_listings);
        rvCartItemListings.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvCartItemListings.setNestedScrollingEnabled(false);
        cartItemLists = new ArrayList<>();
        cartAdapter = new CartAdapter(CartActivity.this, cartItemLists);
        rvCartItemListings.setAdapter(cartAdapter);

        progressBar = findViewById(R.id.progress_bar);
        tvErrorText = findViewById(R.id.tv_error_text);


        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            userViewCartTask();
        } else {
            tvErrorText.setText("No Internet Connection...");
            tvErrorText.setVisibility(View.VISIBLE);
            rvCartItemListings.setVisibility(View.GONE);
        }

        tvItemName = findViewById(R.id.tv_item_name);
        tvItemPrice = findViewById(R.id.tv_item_price);
        tvItemTotalPrice = findViewById(R.id.tv_totalPrice);
        tvWholeTotalPrice = findViewById(R.id.tv_whole_price);


        btnCheckOut = findViewById(R.id.btn_checkOut);
        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cartItemLists.size() > 0) {
                    Intent intent = new Intent(getApplicationContext(), CheckOutActivity.class);
                    intent.putExtra("listcart", cartItemLists);
                    startActivity(intent);
                } else {
                    AlertUtils.showSnack(getApplicationContext(), view, "Empty Cart...");
                }
            }
        });

        btnAddItem = findViewById(R.id.btn_addItems);
        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void userViewCartTask() {
        progressBar.setVisibility(View.VISIBLE);
        tvErrorText.setVisibility(View.GONE);
        rvCartItemListings.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().viewCartApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray cartArray = res.getJSONArray("cart_details");
                                if (cartArray.length() == 0) {
                                    cartMessage();
                                    return;
                                }
                                String imgDirectory = res.getString("profile_image_directory");
                                double wholeTotalPrice = 0.0;
                                for (int i = 0; i < cartArray.length(); i++) {
                                    JSONObject cartObject = cartArray.getJSONObject(i);
                                    CartModel cartModel = new CartModel();
                                    cartModel.setItemCreatorId(cartObject.getString("id"));
                                    cartModel.setCartId(cartObject.getString("cart_id"));
                                    cartModel.setItemName(cartObject.getString("item_name"));
                                    cartModel.setItemId(cartObject.getString("item_id"));
                                    double item_price = Double.parseDouble(cartObject.getString("item_price"));
                                    cartModel.setItemPrice(item_price);
                                    double total_price = Double.parseDouble(cartObject.getString("total_price"));
                                    cartModel.setTotalPrice(total_price);

                                    cartModel.setQty(Integer.parseInt(cartObject.getString("item_quantity")));

                                    if (cartObject.getString("todays_special").equals("1")) {
                                        cartModel.setTodaysSpecial(true);
                                    } else {
                                        cartModel.setTodaysSpecial(false);
                                    }

                                    String itemImg = cartObject.getString("imagepath");
                                    if (!TextUtils.isEmpty(itemImg)) {
                                        cartModel.setItemImage(imgDirectory + itemImg);
                                    }

                                    wholeTotalPrice = wholeTotalPrice + total_price;

                                    cartItemLists.add(cartModel);
                                }

                                tvWholeTotalPrice.setText("$ " + wholeTotalPrice);
                                cartAdapter.notifyDataSetChanged();
                                rvCartItemListings.setVisibility(View.VISIBLE);
                                tvErrorText.setVisibility(View.GONE);
                            } else {
                                tvErrorText.setVisibility(View.VISIBLE);
                                tvErrorText.setText(res.getString("message"));
                                rvCartItemListings.setVisibility(View.GONE);
                            }


                        } catch (JSONException e) {
                            Logger.e("userViewCartTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(getApplicationContext(), tvItemName, errorObj.getString("message"));
                        } catch (Exception e) {
                            Logger.e("userViewCartTask", e.getMessage() + " ");
                        }
                    }
                }, "userViewCartTask");
    }

    public void cartMessage() {
        tvErrorText.setText("No items in cart..");
        rvCartItemListings.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        tvErrorText.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    public void updateWholePrice() {
        Logger.e("updateWholeprice ", "method invoked");
        double total = 0.0;
        for (int i = 0; i < cartItemLists.size(); i++) {
            CartModel cartModel = cartItemLists.get(i);
            total = total + cartModel.getTotalPrice();
        }
        tvWholeTotalPrice.setText("$ " + total);
    }


}
