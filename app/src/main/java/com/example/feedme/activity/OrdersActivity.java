package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.adapter.ItemAdapter;
import com.example.feedme.adapter.OrdersAdapter;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.ItemModel;
import com.example.feedme.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class OrdersActivity extends AppCompatActivity {
    ArrayList<OrderModel> orderLists;
    PreferenceHelper helper;
    RecyclerView orderListings;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView tvErrorText;
    OrdersAdapter ordersAdapter;
    Button btnAddItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_items);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Your Orders");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        helper = PreferenceHelper.getInstance(this);
        btnAddItems = findViewById(R.id.btn_addItems);
        btnAddItems.setVisibility(View.GONE);

        orderListings = findViewById(R.id.list_recycler);
        orderListings.setLayoutManager(new LinearLayoutManager(OrdersActivity.this));
        orderListings.setNestedScrollingEnabled(false);
        orderLists = new ArrayList<>();


        ordersAdapter = new OrdersAdapter(OrdersActivity.this, orderLists);
        orderListings.setAdapter(ordersAdapter);

        tvErrorText = findViewById(R.id.error_text);
        progressBar = findViewById(R.id.progress_bar);

        if (NetworkUtils.isInNetwork(OrdersActivity.this)) {
            if (helper.getUserType().equals("1")) {
                providerOrderListingTask();
            } else {
                customerOrderListingTask();
            }
        } else {
            tvErrorText.setText("No Internet Connection...");
            tvErrorText.setVisibility(View.VISIBLE);
            orderListings.setVisibility(View.GONE);
        }

    }

    private void customerOrderListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        orderListings.setVisibility(View.GONE);
        tvErrorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(OrdersActivity.this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().customerOrderListingApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray customerOrderListingArray = res.getJSONArray("customer_order_details");
                                String profileImgDirectory = res.getString("profile_image_directory");

                                if (customerOrderListingArray.length() > 0) {
                                    for (int i = 0; i < customerOrderListingArray.length(); i++) {
                                        JSONObject orderObject = customerOrderListingArray.getJSONObject(i);
                                        OrderModel orderModel = new OrderModel();
                                        orderModel.setOrderDetails(orderObject.getString("order_id"));
                                        orderModel.setOrderInvoiceId(orderObject.getString("invoice_id"));
                                        orderModel.setOrderDetails(orderObject.getString("order_details"));
                                        orderModel.setOrderProviderName(orderObject.getString("username"));
                                        orderModel.setOrderProviderPhnNo(orderObject.getString("phoneno"));
                                        orderModel.setOrderProviderAddress(orderObject.getString("address"));
                                        orderModel.setPaymentMethod(orderObject.getString("payment_method"));
                                        orderModel.setOrderDate(orderObject.getString("order_date"));

                                        String providerImg = orderObject.getString("user_image");
                                        if (!TextUtils.isEmpty(providerImg)) {
                                            orderModel.setOrderProviderImg(profileImgDirectory + providerImg);
                                        }

                                        String orderStatus = orderObject.getString("status");
                                        orderModel.setOrderStatus(orderStatus);

                                        String total_price = orderObject.getString("total_price");
                                        orderModel.setOrderTotalPrice(Double.parseDouble(total_price));

                                        orderLists.add(orderModel);
                                    }
                                    ordersAdapter.notifyDataSetChanged();
                                    orderListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                } else {
                                    orderListings.setVisibility(View.GONE);
                                    tvErrorText.setVisibility(View.VISIBLE);
                                    tvErrorText.setText("No orders to show");
                                }
                            } else {
                                tvErrorText.setVisibility(View.VISIBLE);
                                tvErrorText.setText(res.getString("message"));
                                orderListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userOrderListingTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("userOrderListingTask error ex", e.getMessage() + " ");
                        }

                    }
                }, "userOrderListingTask");
    }

    private void providerOrderListingTask() {
        progressBar.setVisibility(View.VISIBLE);
        orderListings.setVisibility(View.GONE);
        tvErrorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(OrdersActivity.this);
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("provider_id", helper.getUserId());

        vHelper.addVolleyRequestListeners(Api.getInstance().providerOrderListingTask, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray providerOrderListingArray = res.getJSONArray("provider_order_details");
                                String profileImgDirectory = res.getString("profile_image_directory");

                                if (providerOrderListingArray.length() > 0) {
                                    for (int i = 0; i < providerOrderListingArray.length(); i++) {
                                        JSONObject orderObject = providerOrderListingArray.getJSONObject(i);
                                        OrderModel orderModel = new OrderModel();
                                        orderModel.setOrderId(orderObject.getString("order_id"));
                                        orderModel.setOrderInvoiceId(orderObject.getString("invoice_id"));
                                        orderModel.setOrderDetails(orderObject.getString("order_details"));
                                        orderModel.setOrderProviderName(orderObject.getString("username"));
                                        orderModel.setOrderProviderPhnNo(orderObject.getString("phoneno"));
                                        orderModel.setOrderProviderAddress(orderObject.getString("address"));
                                        orderModel.setPaymentMethod(orderObject.getString("payment_method"));
                                        orderModel.setOrderDate(orderObject.getString("order_date"));

                                        String providerImg = orderObject.getString("user_image");
                                        if (!TextUtils.isEmpty(providerImg)) {
                                            orderModel.setOrderProviderImg(profileImgDirectory + providerImg);
                                        }

                                        String orderStatus = orderObject.getString("status");
                                        orderModel.setOrderStatus(orderStatus);

                                        String total_price = orderObject.getString("total_price");
                                        orderModel.setOrderTotalPrice(Double.parseDouble(total_price));

                                        orderLists.add(orderModel);
                                    }
                                    ordersAdapter.notifyDataSetChanged();
                                    orderListings.setVisibility(View.VISIBLE);
                                    tvErrorText.setVisibility(View.GONE);
                                } else {
                                    orderListings.setVisibility(View.GONE);
                                    tvErrorText.setVisibility(View.VISIBLE);
                                    tvErrorText.setText("No orders to show");
                                }
                            } else {
                                tvErrorText.setVisibility(View.VISIBLE);
                                tvErrorText.setText(res.getString("message"));
                                orderListings.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("providerOrderListingTask json ex", e.getMessage() + "");
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("providerOrderListingTask error ex", e.getMessage() + " ");
                        }

                    }
                }, "providerOrderListingTask");
    }


}

