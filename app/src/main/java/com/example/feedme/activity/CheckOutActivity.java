package com.example.feedme.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.adapter.CheckOutAdapter;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.CartModel;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class CheckOutActivity extends AppCompatActivity {

    ArrayList<CartModel> orderedList;
    //    CartModel cartModel;
    PreferenceHelper helper;

    private static final int AUTOCOMPLETE_REQUEST_CODE = 1002;

    Double total;
    Button placeOrder;
    RadioButton cashRadio;
    Toolbar toolbar;
    TextView subTotalPrice, payByCash, deliveryFee;
    TextView tvErrFullName, tvErrMobile, tvErrAddress;
    AutoCompleteTextView etAddress;
    EditText etMobileNo, etFullName;


    @SuppressLint({"DefaultLocale", "SetTextI18n", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        helper = PreferenceHelper.getInstance(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        toolbar.setTitle("Check Out");

        tvErrAddress = findViewById(R.id.tv_err_address);
        tvErrFullName = findViewById(R.id.tv_err_full_name);
        tvErrMobile = findViewById(R.id.tv_err_mobile);

        orderedList = (ArrayList<CartModel>) getIntent().getSerializableExtra("listcart");

        etMobileNo = findViewById(R.id.et_mobile);
        etMobileNo.setText(helper.getAppUserMobileNo());

        etFullName = findViewById(R.id.et_full_name);
        etFullName.setText(helper.getAppUserFullName());

        etAddress = findViewById(R.id.et_delivery_address);
        etAddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!Places.isInitialized()) {
                        Places.initialize(getApplicationContext(), getResources().getString(R.string.my_api_key));
                    }
                    // Set the fields to specify which types of place data to return.
                    List<Place.Field> fields = Collections.singletonList(Place.Field.ADDRESS);

                    // Start the autocomplete intent.
                    Intent intent = new Autocomplete.IntentBuilder(
                            AutocompleteActivityMode.FULLSCREEN, fields).setCountry("AU")
                            .build(CheckOutActivity.this);
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
                }
                return true;
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.order_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setNestedScrollingEnabled(false);
        CheckOutAdapter checkOutAdapter = new CheckOutAdapter(this, orderedList);
        recyclerView.setAdapter(checkOutAdapter);


        cashRadio = findViewById(R.id.radio_cash);
        cashRadio.setChecked(true);


        subTotalPrice = findViewById(R.id.sub_total_price);
        total = 0.0;
        for (int i = 0; i < orderedList.size(); i++) {
            CartModel cartModel = orderedList.get(i);
            total = total + cartModel.getTotalPrice();
        }
        subTotalPrice.setText("$ " + String.format("%.2f", total));

        deliveryFee = findViewById(R.id.delivery_price);
        //setting delivery fee to zero
        deliveryFee.setText("$ 0.00");

        payByCash = findViewById(R.id.tv_total_price);
        payByCash.setText("$ " + String.format("%.2f", total));


        cashRadio = findViewById(R.id.radio_cash);

        placeOrder = findViewById(R.id.btn_place_order);
        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        userPlaceOrderTask();
                    } else {
                        AlertUtils.showSnack(CheckOutActivity.this, view, "No Internet Connection...");
                    }
                }
            }
        });
    }

    private void userPlaceOrderTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(CheckOutActivity.this, "Placing Order...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        double total_cost = 0.0;
        String orderDetails = "";
        String providerId = "";

        for (int i = 0; i < orderedList.size(); i++) {
            CartModel cartModel = orderedList.get(i);
            orderDetails = orderDetails + cartModel.getQty() + "    X    " + cartModel.getItemName()
                    + "         $  " + cartModel.getItemPrice() + "\n";
            total_cost = total_cost + cartModel.getTotalPrice();

        }

        providerId = orderedList.get(0).getItemCreatorId();
        Logger.e("providerId", providerId);

        //for viewing the parsed string of customer's ordered item details and price.
        Logger.e("orderDetails ", orderDetails);

        postParams.put("provider_id", providerId);
        postParams.put("username", helper.getAppUserFullName());
        postParams.put("phoneno", helper.getAppUserMobileNo());
        postParams.put("address", etAddress.getText().toString().trim());
        postParams.put("order_details", orderDetails);
        postParams.put("payment_method", "Cash on Delivery");
        postParams.put("total_price", String.valueOf(total_cost));

        vHelper.addVolleyRequestListeners(Api.getInstance().placeOrderApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertUtils.simpleAlert(CheckOutActivity.this, false, "Success", "Your order has been placed successfully",
                                        "View Orders", "", new AlertUtils.OnAlertButtonClickListener() {
                                            @Override
                                            public void onAlertButtonClick(boolean isPositiveButton) {
                                                Intent intent = new Intent(getApplicationContext(), OrdersActivity.class);
                                                startActivity(intent);
                                            }
                                        });
                            } else {
                                AlertUtils.showToast(CheckOutActivity.this, res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("userPlaceOrderTask json ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("userPlaceOrderTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(CheckOutActivity.this, "Error", errorMsg,
                                "OK", "", null);
                    }
                }, "userPlaceOrderTask");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Logger.e("tag", "address: " + place.getAddress());
                etAddress.setText(place.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Logger.e("message", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    public boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
            isValid = false;
            tvErrAddress.setText("Please select address");
            tvErrAddress.setVisibility(View.VISIBLE);
        } else {
            tvErrAddress.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(etFullName.getText().toString().trim())) {
            isValid = false;
            tvErrFullName.setText("Full name cannot be empty");
            tvErrFullName.setVisibility(View.VISIBLE);
        } else {
            tvErrFullName.setVisibility(View.GONE);
        }

        if (etMobileNo.getText().toString().trim().equals("")) {
            isValid = false;
            tvErrMobile.setVisibility(View.VISIBLE);
            tvErrMobile.setText("This Field is required...");
        } else {
            if (etMobileNo.getText().toString().trim().length() < 10) {
                isValid = false;
                tvErrMobile.setText("The mobile must be equal to 10 numbers");
                tvErrMobile.setVisibility(View.VISIBLE);
            } else {
                tvErrMobile.setVisibility(View.GONE);
            }
        }


        return isValid;
    }
}
