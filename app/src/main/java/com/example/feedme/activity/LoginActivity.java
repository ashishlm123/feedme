package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    EditText etEmailAddress, etPassword;
    PreferenceHelper helper;
    TextView tvForgotPassword, tvSignUp, tvErrEmail, tvErrPass;

    Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = PreferenceHelper.getInstance(this);
        setContentView(R.layout.activity_login);

        etEmailAddress = findViewById(R.id.et_email_address);
        etPassword = findViewById(R.id.et_password);

        tvForgotPassword = findViewById(R.id.tv_forgot_password);
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ForgetPasswordActivity.class));
            }
        });
        tvSignUp = findViewById(R.id.tv_sign_up);
        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
            }
        });
        tvErrEmail = findViewById(R.id.tv_error_email);
        tvErrPass = findViewById(R.id.tv_error_password);

        btnSignIn = findViewById(R.id.btn_login);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                        userLoginTask();
                    } else {
                        AlertUtils.showSnack(LoginActivity.this, btnSignIn, "Please try again with internet connection.");
                    }
                }
            }
        });
    }

    //for checking the validation of login form
    public boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(etEmailAddress.getText().toString().trim())) {
            isValid = false;
            tvErrEmail.setVisibility(View.VISIBLE);
            tvErrEmail.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.getText().toString().trim()).matches()) {
                tvErrEmail.setVisibility(View.GONE);
            } else {
                tvErrEmail.setText("Invalid Email Address");
                tvErrEmail.setVisibility(View.VISIBLE);
            }
        }

        if (etPassword.getText().toString().trim().equals("")) {
            isValid = false;
            tvErrPass.setVisibility(View.VISIBLE);
            tvErrPass.setText("This Field is required...");
        } else {
            if (etPassword.getText().toString().trim().length() < 6) {
                etPassword.setText("The password must be more than 6 characters");
            } else {
                tvErrPass.setVisibility(View.GONE);
            }
        }

        return isValid;
    }

    private void userLoginTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(LoginActivity.this, "Logging In...");

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("email", etEmailAddress.getText().toString().trim());
        postParams.put("password", etPassword.getText().toString().trim());

        vHelper.addVolleyRequestListeners(Api.getInstance().loginApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        String profileImgDirectory = res.getString("profile_image_directory");
                        JSONObject user = res.getJSONObject("user_detail");
                        String profileImg = user.getString("user_image");
                        helper.edit().putString(PreferenceHelper.USER_FULL_NAME, user.getString("username")).apply();
                        helper.edit().putString(PreferenceHelper.USER_ID, user.getString("id")).apply();
                        helper.edit().putString(PreferenceHelper.USER_EMAIL, user.getString("email")).apply();
                        helper.edit().putString(PreferenceHelper.USER_ADDRESS, user.getString("address")).apply();
                        helper.edit().putString(PreferenceHelper.USER_TYPE, user.getString("usertype")).apply();
                        helper.edit().putString(PreferenceHelper.USER_MOBILE_NO, user.getString("phoneno")).apply();
                        helper.edit().putString(PreferenceHelper.USER_IMG, profileImgDirectory + profileImg).apply();
                        helper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).apply();

                        if (user.getString("usertype").equals("1")) {
                            helper.edit().putString(PreferenceHelper.USER_AVAILABILITY, user.getString("availability")).apply();
                            helper.edit().putString(PreferenceHelper.FOOD_CERTIFICATE, user.getString("foodcertificate")).apply();
                            Intent intent = new Intent(getApplicationContext(), ProviderProfileActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                    } else {
                        progressDialog.dismiss();
                        AlertUtils.showSnack(LoginActivity.this, tvForgotPassword, res.getString("message"));
                    }

                } catch (JSONException e) {
                    Logger.e("userLoginTask json ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected Error, Please try again with internet access!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("userLoginTask error ex", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(LoginActivity.this, "Error", errorMsg,
                        "OK", "", null);
            }
        }, "userLoginTask");

    }
}
