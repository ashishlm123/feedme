package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgetPasswordActivity extends AppCompatActivity {
    EditText etEmail;
    TextView tvErrEmail;
    Button btnForgotPassword;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Forgot Password");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnForgotPassword = findViewById(R.id.btn_forget_password);
        tvErrEmail = findViewById(R.id.tv_error_email);
        etEmail = findViewById(R.id.et_email_address);

        btnForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        userForgotPasswordTask();
                    } else {
                        AlertUtils.showSnack(ForgetPasswordActivity.this, btnForgotPassword, "Please, try again with internet access");
                    }
                }
            }
        });
    }

    private void userForgotPasswordTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(ForgetPasswordActivity.this, "Sending Password...");

        VolleyHelper volleyHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = volleyHelper.getPostParams();
        postParams.put("email", etEmail.getText().toString().trim());

        volleyHelper.addVolleyRequestListeners(Api.getInstance().forgetPasswordApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    final JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.simpleAlert(ForgetPasswordActivity.this, "New Password",
                                "Your new password is " + res.getString("password"), "Copy Password", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        if (isPositiveButton) {
                                            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                                            ClipData clip = null;
                                            try {
                                                clip = ClipData.newPlainText("password", res.getString("password"));
                                                assert clipboard != null;
                                                clipboard.setPrimaryClip(clip);

                                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                                                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                            } catch (JSONException e) {
                                                Logger.e("copy exception", e.getMessage() + "");
                                            }

                                        }
                                    }
                                });
                    } else {
                        AlertUtils.showSnack(ForgetPasswordActivity.this, btnForgotPassword, res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("userForgotPasswordTask error ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                progressDialog.dismiss();
                String errorMsg = "Unexpected Error, Please try again with internet access!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (JSONException e) {
                    Logger.e("userForgotPasswordTask error ex", e.getMessage() + " ");
                }
                AlertUtils.simpleAlert(ForgetPasswordActivity.this, "Error", errorMsg,
                        "OK", "", null);

            }
        }, "userForgotPasswordTask");
    }

    private boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            isValid = false;
            tvErrEmail.setVisibility(View.VISIBLE);
            tvErrEmail.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                tvErrEmail.setVisibility(View.GONE);
            } else {
                isValid = false;
                tvErrEmail.setText("Invalid Email Address");
                tvErrEmail.setVisibility(View.VISIBLE);
            }
        }
        return isValid;
    }
}
