package com.example.feedme.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.ImageHelper;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.Utils;
import com.example.feedme.helper.VolleyHelper;
import com.example.feedme.model.ItemModel;
import com.google.gson.JsonObject;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class AddItemsActivity extends AppCompatActivity {
    PreferenceHelper helper;

    ImageView ivItemImage;
    EditText etItemName, etItemPrice;
    TextView tvErrItemName, tvErrItemPrice, tvErrSpecial, tvErrItemImage;
    Spinner spTodaySpecial;
    Button btnAddItem;

    String selectedImagePath;
    public static Uri fileUri = null;
    public static final int RESULT_LOAD_IMAGE = 123;
    public static final int RESULT_LOAD_VIDEO = 124;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 9873;


    @SuppressLint({"DefaultLocale", "SetTextI18n", "ResourceType"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_items);
        helper = PreferenceHelper.getInstance(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Add New Item");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Intent intent = getIntent();
        ItemModel itemModel = (ItemModel) intent.getSerializableExtra("item");
        assert itemModel != null;
        boolean isEdit = intent.getBooleanExtra("isEdit", false);

        tvErrItemName = findViewById(R.id.tv_err_item_name);
        tvErrItemPrice = findViewById(R.id.tv_err_item_price);
        tvErrSpecial = findViewById(R.id.tv_err_special);
        tvErrItemImage = findViewById(R.id.tv_err_item_image);

        etItemName = findViewById(R.id.et_item_name);
        etItemPrice = findViewById(R.id.et_item_price);

        ivItemImage = findViewById(R.id.item_icon);
        ivItemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertForPhoto();
            }
        });

        spTodaySpecial = findViewById(R.id.sp_today_special);
        ArrayAdapter<String> todaySpecialArrayAdapter = new ArrayAdapter<>(AddItemsActivity.this, R.layout.item_spinner,
                getResources().getStringArray(R.array.special)); //selected item will look like a spinner set from XML
        todaySpecialArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTodaySpecial.setAdapter(todaySpecialArrayAdapter);

        if (isEdit) {
            etItemPrice.setText("$ " + String.format("%.2f", itemModel.getItemPrice()));
            etItemName.setText(itemModel.getItemName());

            if (itemModel.isTodaysSpecial()) {
                spTodaySpecial.setSelection(1);
            } else {
                spTodaySpecial.setSelection(0);
            }

            if (!TextUtils.isEmpty(itemModel.getItemImage())) {
                Picasso.with(AddItemsActivity.this).load(itemModel.getItemImage()).into(ivItemImage);
            }
        }

        btnAddItem = findViewById(R.id.btn_save_item_details);
        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(AddItemsActivity.this)) {
                        userAddItemsTask();
                    } else {
                        AlertUtils.showSnack(AddItemsActivity.this, btnAddItem, "Please, try again with Internet Access.");
                    }
                }
            }
        });

    }

    private void alertForPhoto() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(AddItemsActivity.this);
        builder.setTitle("Choose Media");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(AddItemsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AddItemsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }

                } else if (options[item].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(AddItemsActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(AddItemsActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RESULT_LOAD_IMAGE);
                    } else {
                        chooseFrmGallery(false);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(boolean video) {
        Logger.e("choose from gallery ma pugis", "ah yaar");
        if (video) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);
        }
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_feed_me");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_feed_me" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded gallary", selImgPath);
//                selectedImagePath = selImgPath;
                CropImage.activity(selectedImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(1, 1)
                        .start(AddItemsActivity.this);

            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
//                selectedImagePath = fileUri.getPath();
//                correctOrientation(selectedImagePath);
//                Logger.e("imagePath camera capture", selectedImagePath);

                if (fileUri != null) {
                    CropImage.activity(fileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(1, 1)
                            .start(AddItemsActivity.this);
                } else {
                    return;
                }

            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                selectedImagePath = resultUri.getPath();
                if (resultUri.getPath() != null) {
                    Picasso.with(AddItemsActivity.this).load(resultUri).into(ivItemImage);
                } else {

                }
            }
        }
    }

    public boolean validation() {
        //validation form whether all the required inputs has been entered or not.
        boolean isValid = true;

        if (TextUtils.isEmpty(etItemName.getText().toString().trim())) {
            isValid = false;
            tvErrItemName.setText("Please enter Item Name");
            tvErrItemName.setVisibility(View.VISIBLE);
        } else {
            tvErrItemName.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(etItemPrice.getText().toString().trim())) {
            isValid = false;
            tvErrItemPrice.setText("Please enter Item Price");
            tvErrItemPrice.setVisibility(View.VISIBLE);
        } else {
            tvErrItemPrice.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(selectedImagePath)) {
            isValid = false;
            tvErrItemImage.setText("Please select Item Image");
            tvErrItemImage.setVisibility(View.VISIBLE);
        } else {
            tvErrItemImage.setVisibility(View.GONE);
        }

        return isValid;
    }

    private void userAddItemsTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(AddItemsActivity.this, "Adding Item...");
        VolleyHelper vHelper = VolleyHelper.getInstance(AddItemsActivity.this);
        HashMap<String, String> postParams = vHelper.getPostParams();

        //Encoding selected image or capture photo into base 64VII to send image through web services.
        String encodedImage = ImageHelper.encodeTobase64VII(Objects.requireNonNull(ImageHelper.decodeImageFileWithCompression(selectedImagePath,
                (int) Utils.pxFromDp(AddItemsActivity.this, 300f), (int) Utils.pxFromDp(AddItemsActivity.this, 300f))));

        postParams.put("encoded_string", encodedImage);
        postParams.put("item_name", etItemName.getText().toString().trim().replace("$", ""));
        postParams.put("item_price", etItemPrice.getText().toString().trim());

        if (spTodaySpecial.getSelectedItemPosition() == 0) {
            postParams.put("todays_special", "0");
        } else if (spTodaySpecial.getSelectedItemPosition() == 1) {
            //selection logic of items placed in a spinner data for "Today's Special".
            postParams.put("todays_special", "1");
        } else {
            postParams.put("todays_special", "0");
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().addnewItemApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.simpleAlert(AddItemsActivity.this, false, "Success",
                                "Item Added Sucessfully.", "View Items", "", new AlertUtils.OnAlertButtonClickListener() {
                                    @Override
                                    public void onAlertButtonClick(boolean isPositiveButton) {
                                        Intent intent = new Intent(getApplicationContext(), ViewItemsActivity.class);
                                        startActivity(intent);
                                    }
                                });
                    } else {
                        AlertUtils.showSnack(AddItemsActivity.this, btnAddItem, res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("userAddItemsTask ex: ", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {

            }
        }, "userAddItemsTask");

    }

}
