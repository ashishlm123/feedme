package com.example.feedme.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.adapter.DayAdapter;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.ImageHelper;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.Utils;
import com.example.feedme.helper.VolleyHelper;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class SignUpActivity extends AppCompatActivity {
    PreferenceHelper helper;
    EditText etEmail, etFullName, etPassword, etConfirmPassword, etMobile, etCertificate;
    Spinner spUserType;
    Button btnCompleteRegistration;
    TextView tvErrEmail, tvErrFullName, tvErrPassword, tvConfirmPassword, tvErrMobile,
            tvErrCertificate, tvErrUserType, tvErrAddress, tvErrAvailability, btnAddAvailability, tvErrProfilePic;
    ImageView ivProfilePic;

    TextView daySun, dayMon, dayTue, dayWed, dayThur, dayFri, daySat;
    private static final int AUTOCOMPLETE_REQUEST_CODE = 1002;
    AutoCompleteTextView etAddress;

    LinearLayout layCertificate, layAvailability;

    String selectedImagePath;
    public static Uri fileUri = null;
    public static final int RESULT_LOAD_IMAGE = 123;
    public static final int RESULT_LOAD_VIDEO = 124;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 125;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        helper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Sign Up");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        itemInitialization();

        ivProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertForPhoto();
            }
        });

        etAddress = findViewById(R.id.et_address);
        etAddress.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (!Places.isInitialized()) {
                        Places.initialize(getApplicationContext(), getResources().getString(R.string.my_api_key));
                    }
                    // Set the fields to specify which types of place data to return.
                    List<Place.Field> fields = Collections.singletonList(Place.Field.ADDRESS);

                    // Start the autocomplete intent.
                    Intent intent = new Autocomplete.IntentBuilder(
                            AutocompleteActivityMode.FULLSCREEN, fields).setCountry("AU")
                            .build(SignUpActivity.this);
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
                }
                return true;
            }
        });

        ArrayAdapter<String> userTypeArrayAdapter = new ArrayAdapter<>(this, R.layout.item_spinner,
                getResources().getStringArray(R.array.user_types)); //selected item will look like a spinner set from XML
        userTypeArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spUserType.setAdapter(userTypeArrayAdapter);

        spUserType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    layAvailability.setVisibility(View.GONE);
                    layCertificate.setVisibility(View.GONE);
                } else if (position == 1) {
                    layAvailability.setVisibility(View.GONE);
                    layCertificate.setVisibility(View.GONE);
                } else if (position == 2) {
                    layAvailability.setVisibility(View.VISIBLE);
                    layCertificate.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnAddAvailability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                availabilityAlert();
            }
        });

        btnCompleteRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validation()) {
                    if (NetworkUtils.isInNetwork(SignUpActivity.this)) {
                        userRegistrationTask();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Logger.e("tag", "address: " + place.getAddress());
                etAddress.setText(place.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Logger.e("message", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_LOAD_IMAGE) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selImgPath = cursor.getString(column_index);
                Logger.e("imagePath loaded gallary", selImgPath);
//                selectedImagePath = selImgPath;
                CropImage.activity(selectedImageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setFixAspectRatio(true)
                        .setAspectRatio(1, 1)
                        .start(SignUpActivity.this);

            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
//                selectedImagePath = fileUri.getPath();
//                correctOrientation(selectedImagePath);
//                Logger.e("imagePath camera capture", selectedImagePath);

                if (fileUri != null) {
                    CropImage.activity(fileUri)
                            .setGuidelines(CropImageView.Guidelines.ON)
                            .setFixAspectRatio(true)
                            .setAspectRatio(1, 1)
                            .start(SignUpActivity.this);
                } else {
                    return;
                }

            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Uri resultUri = result.getUri();
                selectedImagePath = resultUri.getPath();
                if (resultUri.getPath() != null) {
                    Picasso.with(SignUpActivity.this).load(resultUri).into(ivProfilePic);
                } else {

                }
            }
        }
    }

    private void userRegistrationTask() {
        final ProgressDialog progressDialog = AlertUtils.showProgressDialog(SignUpActivity.this, "Completing Registration...");
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = vHelper.getPostParams();
        postParams.put("username", etFullName.getText().toString().trim());
        postParams.put("email", etEmail.getText().toString().trim());
        postParams.put("address", etAddress.getText().toString().trim());
        postParams.put("phoneno", etMobile.getText().toString().trim());
        postParams.put("password", etPassword.getText().toString().trim());

        String encodedImage = ImageHelper.encodeTobase64VII(Objects.requireNonNull(ImageHelper.decodeImageFileWithCompression(selectedImagePath,
                (int) Utils.pxFromDp(SignUpActivity.this, 300f), (int) Utils.pxFromDp(SignUpActivity.this, 300f))));

        postParams.put("encoded_string", encodedImage);

        if (spUserType.getSelectedItemPosition() == 1) {
            postParams.put("usertype", "0");
            postParams.put("availability", "");
            postParams.put("foodcertificate", "");
        } else if (spUserType.getSelectedItemPosition() == 2) {
            postParams.put("usertype", "1");
            postParams.put("availability", btnAddAvailability.getText().toString().trim());
            postParams.put("foodcertificate", etCertificate.getText().toString().trim());
        }


        vHelper.addVolleyRequestListeners(Api.getInstance().registerApi, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                String profileImgDirectory = res.getString("profile_image_directory");
                                JSONObject user = res.getJSONObject("user_detail");
                                String profileImg = user.getString("user_image");
                                helper.edit().putString(PreferenceHelper.USER_FULL_NAME, user.getString("username")).apply();
                                helper.edit().putString(PreferenceHelper.USER_ID, user.getString("id")).apply();
                                helper.edit().putString(PreferenceHelper.USER_EMAIL, user.getString("email")).apply();
                                helper.edit().putString(PreferenceHelper.USER_MOBILE_NO, user.getString("phoneno")).apply();
                                helper.edit().putString(PreferenceHelper.USER_ADDRESS, user.getString("address")).apply();
                                helper.edit().putString(PreferenceHelper.USER_TYPE, user.getString("usertype")).apply();

                                helper.edit().putString(PreferenceHelper.USER_IMG, profileImgDirectory + profileImg).apply();

                                helper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).apply();

                                if (user.getString("usertype").equals("1")) {
                                    helper.edit().putString(PreferenceHelper.USER_AVAILABILITY, user.getString("availability")).apply();
                                    helper.edit().putString(PreferenceHelper.FOOD_CERTIFICATE, user.getString("foodcertificate")).apply();
                                    Intent intent = new Intent(getApplicationContext(), ProviderProfileActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            } else {
                                AlertUtils.showSnack(SignUpActivity.this, btnAddAvailability, res.getString("message"));
                            }

                        } catch (JSONException e) {
                            Logger.e("userRegistrationTask json ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        progressDialog.dismiss();
                        String errorMsg = "Unexpected Error, Please try again with internet access!";
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (JSONException e) {
                            Logger.e("userRegistrationTask error ex", e.getMessage() + " ");
                        }
                        AlertUtils.simpleAlert(SignUpActivity.this, "Error", errorMsg,
                                "OK", "", null);
                    }
                }, "userRegistrationTask");

    }

    private void itemInitialization() {
        ivProfilePic = findViewById(R.id.iv_profile_pic);
        tvErrProfilePic = findViewById(R.id.tv_err_profile_pic);

        etEmail = findViewById(R.id.et_email_address);
        etFullName = findViewById(R.id.et_full_name);
        etPassword = findViewById(R.id.et_password);
        etConfirmPassword = findViewById(R.id.et_confirm_password);
        etMobile = findViewById(R.id.et_mobile);
        etCertificate = findViewById(R.id.et_certificate);

        layAvailability = findViewById(R.id.layout_availability);
        layCertificate = findViewById(R.id.layout_certificate);

        spUserType = findViewById(R.id.sp_user_type);
        btnAddAvailability = findViewById(R.id.btn_add_availability);

        btnCompleteRegistration = findViewById(R.id.btn_complete_registration);

        tvErrEmail = findViewById(R.id.tv_error_email);
        tvErrPassword = findViewById(R.id.tv_error_password);
        tvConfirmPassword = findViewById(R.id.tv_confirm_password);
        tvErrMobile = findViewById(R.id.tv_err_mobile);
        tvErrFullName = findViewById(R.id.tv_err_full_name);
        tvErrCertificate = findViewById(R.id.tv_err_certificate);
        tvErrUserType = findViewById(R.id.tv_err_user_type);
        tvErrAvailability = findViewById(R.id.tv_err_availability);
        tvErrAddress = findViewById(R.id.tv_err_address);
    }

    public boolean validation() {
        boolean isValid = true;

        if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
            isValid = false;
            tvErrEmail.setVisibility(View.VISIBLE);
            tvErrEmail.setText("This Field is required...");
        } else {
            if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                tvErrEmail.setVisibility(View.GONE);
            } else {
                isValid = false;
                tvErrEmail.setText("Invalid Email Address");
                tvErrEmail.setVisibility(View.VISIBLE);
            }
        }

        if (TextUtils.isEmpty(etFullName.getText().toString().trim())) {
            isValid = false;
            tvErrFullName.setText("Please select address");
            tvErrFullName.setVisibility(View.VISIBLE);
        } else {
            tvErrFullName.setVisibility(View.GONE);
        }

        if (etPassword.getText().toString().trim().equals("")) {
            isValid = false;
            tvErrPassword.setVisibility(View.VISIBLE);
            tvErrPassword.setText("This Field is required...");
        } else {
            if (etPassword.getText().toString().trim().length() < 6) {
                etPassword.setText("The password must be more than 6 characters");
            } else {
                tvErrPassword.setVisibility(View.GONE);
            }
        }

        if (!etPassword.getText().toString().trim().equals(etConfirmPassword.getText().toString().trim())) {
            isValid = false;
            tvErrPassword.setText("Password does not match");
            tvErrPassword.setVisibility(View.VISIBLE);
        } else {
            tvErrPassword.setVisibility(View.GONE);
        }

        if (etMobile.getText().toString().trim().equals("")) {
            isValid = false;
            tvErrMobile.setVisibility(View.VISIBLE);
            tvErrMobile.setText("This Field is required...");
        } else {
            if (etMobile.getText().toString().trim().length() < 10) {
                tvErrMobile.setText("The mobile must be equal to 10 numbers");
                tvErrMobile.setVisibility(View.VISIBLE);
            } else {
                tvErrMobile.setVisibility(View.GONE);
            }
        }

        if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
            isValid = false;
            tvErrAddress.setText("Please select address");
            tvErrAddress.setVisibility(View.VISIBLE);
        } else {
            tvErrAddress.setVisibility(View.GONE);
        }

        if (spUserType.getSelectedItem().toString().equalsIgnoreCase("Provider")) {
            if (TextUtils.isEmpty(etCertificate.getText().toString().trim())) {
                isValid = false;
                tvErrCertificate.setText("Please enter Certificate Link");
                tvErrCertificate.setVisibility(View.VISIBLE);
            } else {
                tvErrCertificate.setVisibility(View.GONE);
            }

            if (TextUtils.isEmpty(btnAddAvailability.getText().toString().trim())) {
                isValid = false;
                tvErrAvailability.setText("Please enter Availability");
                tvErrAvailability.setVisibility(View.VISIBLE);
            } else {
                tvErrAvailability.setVisibility(View.GONE);
            }
        }

        if (spUserType.getSelectedItem().toString().trim().equalsIgnoreCase("Select User Type")) {
            isValid = false;
            tvErrUserType.setText("Please select User Type");
            tvErrUserType.setVisibility(View.VISIBLE);
        } else {
            tvErrUserType.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(selectedImagePath)) {
            isValid = false;
            tvErrProfilePic.setText("Please select profile picture");
            tvErrProfilePic.setVisibility(View.VISIBLE);
        } else {
            tvErrProfilePic.setVisibility(View.GONE);
        }

        return isValid;
    }

    private void availabilityAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(SignUpActivity.this);
        View view = LayoutInflater.from(SignUpActivity.this).inflate(R.layout.alert_availability, null, false);
        dialog.setView(view);

        RecyclerView rvDays = view.findViewById(R.id.list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvDays.setNestedScrollingEnabled(false);
        rvDays.setLayoutManager(layoutManager);
        String[] dayLists = getResources().getStringArray(R.array.days);
        final DayAdapter dayAdapter = new DayAdapter(SignUpActivity.this, dayLists);
        rvDays.setAdapter(dayAdapter);


        final Dialog dialog1 = dialog.show();
        Button cancel = view.findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });

        Button update = view.findViewById(R.id.btn_update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Logger.e("Selected Days", dayAdapter.getSelectedArray());
                String selectedDays = dayAdapter.getSelectedArray();
                if (selectedDays.equals("")) {
                    AlertUtils.showSnack(SignUpActivity.this, btnAddAvailability, "Please select your availability");
                } else {
                    btnAddAvailability.setText(selectedDays);
                    dialog1.dismiss();
                }
            }
        });
    }

    private void alertForPhoto() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SignUpActivity.this);
        builder.setTitle("Choose Media");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    } else {
                        takePhoto();
                    }

                } else if (options[item].equals("Choose From Gallery")) {
                    if (ContextCompat.checkSelfPermission(SignUpActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(SignUpActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RESULT_LOAD_IMAGE);
                    } else {
                        chooseFrmGallery(false);
                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void chooseFrmGallery(boolean video) {
        Logger.e("choose from gallery ma pugis", "ah yaar");
        if (video) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_VIDEO);
        } else {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

            startActivityForResult(i, RESULT_LOAD_IMAGE);
        }
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(),
                "file_feed_me");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + "file_feed_me" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

}
