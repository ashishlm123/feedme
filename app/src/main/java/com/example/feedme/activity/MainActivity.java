package com.example.feedme.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.feedme.R;
import com.example.feedme.fragment.ContactUsFrag;
import com.example.feedme.fragment.HomeFragment;
import com.example.feedme.fragment.NearbyProvidersFrag;
import com.example.feedme.helper.AlertUtils;
import com.example.feedme.helper.Api;
import com.example.feedme.helper.Logger;
import com.example.feedme.helper.NetworkUtils;
import com.example.feedme.helper.PreferenceHelper;
import com.example.feedme.helper.VolleyHelper;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
    FragmentManager fm;
    PreferenceHelper prefsHelper;

    NavigationView navigationView;
    View views;
    MenuItem mPreviousMenuItem;
    Toolbar toolbar;
    TextView tvUserFullName, tvUserEmail;
    TextView tvCartItemCount;
    ImageView ivProfilePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fm = getSupportFragmentManager();
        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());

        toolbar = findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.main);

        Menu menu = toolbar.getMenu();
        MenuItem menuCart = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(menuCart, R.layout.badge_layout);
        View view = MenuItemCompat.getActionView(menuCart);
        tvCartItemCount = view.findViewById(R.id.tv_cart_item_count);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);
            }
        });


        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.inflateMenu(R.menu.activity_main_drawer);

        views = navigationView.getHeaderView(0);
        navigationView.setNavigationItemSelectedListener(this);

        setHeaderData();

        //default home fragment
        fm.beginTransaction().add(R.id.content_main, new HomeFragment(), "Home").commit();
        setTitle();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            userCartCountTask();
        }
    }

    public void setHeaderData() {
        tvUserFullName = views.findViewById(R.id.tv_user_name);
        tvUserFullName.setText(prefsHelper.getAppUserFullName());

        tvUserEmail = views.findViewById(R.id.tv_user_email);
        tvUserEmail.setText(prefsHelper.getUserEmail());

        ivProfilePic = views.findViewById(R.id.iv_profile_pic);
        Logger.e("link", prefsHelper.getAppUserImg());
        if (!TextUtils.isEmpty(prefsHelper.getAppUserImg())) {
            Picasso.with(MainActivity.this).load(prefsHelper.getAppUserImg()).into(ivProfilePic);
        }
    }

    @Override
    public void onBackPressed() {
        setTitle();
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = fm.findFragmentById(R.id.content_main);
            if (fragment instanceof HomeFragment) {
                AlertUtils.simpleAlert(MainActivity.this, "Exit Application", " Are you sure you want to close the application",
                        "Yes", "No", new AlertUtils.OnAlertButtonClickListener() {
                            @Override
                            public void onAlertButtonClick(boolean isPositiveButton) {
                                if (isPositiveButton) {
                                    finish();
                                }
                            }
                        });
            } else {
                super.onBackPressed();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        item.setCheckable(true);
        item.setChecked(true);
        if (mPreviousMenuItem != null) {
            mPreviousMenuItem.setChecked(false);
        }
        mPreviousMenuItem = item;

        int id = item.getItemId();
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        switch (id) {
            case R.id.nav_home:
                fragment = new HomeFragment();
                break;

            case R.id.nav_cart:
                Intent cartActivity = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(cartActivity);
                break;

            case R.id.nav_orders:
                Intent orderActivity = new Intent(getApplicationContext(), OrdersActivity.class);
                startActivity(orderActivity);
                break;

            case R.id.nav_provider:
                fragment = new NearbyProvidersFrag();
                break;

            case R.id.nav_contact_us:
                fragment = new ContactUsFrag();
                break;

            case R.id.nav_sign_out:
                prefsHelper.edit().clear().apply();
                Intent loginActivity = new Intent(getApplicationContext(), LoginActivity.class);
                loginActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                prefsHelper.edit().clear().apply();
                startActivity(loginActivity);
                break;
        }

        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_main, fragment)
                    .addToBackStack(null)
                    .commit();
            drawer.closeDrawer(GravityCompat.START);
        }
        setTitle();
        return true;
    }

    public void setTitle() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = fm.findFragmentById(R.id.content_main);
                if (fragment instanceof HomeFragment) {
                    toolbar.setTitle("Home");
                } else if (fragment instanceof ContactUsFrag) {
                    toolbar.setTitle("Contact Us");
                } else if (fragment instanceof NearbyProvidersFrag) {
                    toolbar.setTitle("Providers");
                } else {
                    toolbar.setTitle(getString(R.string.app_name));
                }
            }
        }, 200);
    }

    private void userCartCountTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = vHelper.getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().viewCartApi, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray cartArray = res.getJSONArray("cart_details");
                                if (cartArray.length() == 0) {
                                    tvCartItemCount.setVisibility(View.GONE);
                                } else {
                                    tvCartItemCount.setText(String.valueOf(cartArray.length()));
                                    tvCartItemCount.setVisibility(View.VISIBLE);
                                }
                            } else {
                                tvCartItemCount.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            Logger.e("userCartCountTask json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                            AlertUtils.showSnack(getApplicationContext(), tvUserEmail, errorObj.getString("message"));
                        } catch (Exception e) {
                            Logger.e("userCartCountTask", e.getMessage() + " ");
                        }
                    }
                }, "userCartCountTask");
    }


}
