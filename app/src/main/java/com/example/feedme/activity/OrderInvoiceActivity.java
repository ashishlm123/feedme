package com.example.feedme.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.feedme.R;
import com.example.feedme.model.OrderModel;
import com.squareup.picasso.Picasso;

public class OrderInvoiceActivity extends AppCompatActivity {
    TextView tvInvoiceId, tvName, tvOrderItems, tvTotalPrice, tvOrderDate, tvPictureText, tvDeliveryAddress;
    Toolbar toolbar;
    OrderModel orderModel;
    boolean isProvider = false;
    ImageView ivProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_invoice);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitle("Invoice");

        Intent intent = getIntent();
        orderModel = (OrderModel) intent.getSerializableExtra("order");
        isProvider = intent.getBooleanExtra("isProvider", false);


        tvInvoiceId = findViewById(R.id.tv_invoice_id);
        tvName = findViewById(R.id.tv_name);
        tvOrderItems = findViewById(R.id.tv_ordered_items);
        tvTotalPrice = findViewById(R.id.tv_total_price);
        tvOrderDate = findViewById(R.id.tv_order_date);
        tvPictureText = findViewById(R.id.tv_picture_text);
        ivProvider = findViewById(R.id.iv_icon);
        tvDeliveryAddress = findViewById(R.id.tv_delivery_address);

        if (isProvider) {
            //For showing the customer's information when provider is viewing the invoice with order details
            tvName.setText("Customer Name: " + orderModel.getOrderProviderName());
            tvInvoiceId.setText("Invoice ID:\n" + orderModel.getOrderInvoiceId());
            tvTotalPrice.setText("$ " + orderModel.getOrderTotalPrice());
            tvOrderItems.setText(orderModel.getOrderDetails());
            tvOrderDate.setText("Ordered Date:\n" + orderModel.getOrderDate());
            tvPictureText.setText("Customer Picture:");
            if (!TextUtils.isEmpty(orderModel.getOrderProviderImg())) {
                Picasso.with(OrderInvoiceActivity.this).load(orderModel.getOrderProviderImg()).into(ivProvider);
            }

            //It fetches the delivery address that customer has provided. We used same model for both customer and provider
            tvDeliveryAddress.setText("Delivery Address: " + orderModel.getOrderProviderAddress());
            tvDeliveryAddress.setVisibility(View.VISIBLE);
        } else {
            //For showing the customer's information when provider is viewing the invoice with order details
            tvName.setText("Provider Name: " + orderModel.getOrderProviderName());
            tvInvoiceId.setText("Invoice ID: " + orderModel.getOrderInvoiceId());
            tvTotalPrice.setText("$ " + orderModel.getOrderTotalPrice());
            tvOrderItems.setText(orderModel.getOrderDetails());
            tvOrderDate.setText("Date: " + orderModel.getOrderDate());
            tvPictureText.setText("Provider Picture:");
            if (!TextUtils.isEmpty(orderModel.getOrderProviderImg())) {
                Picasso.with(OrderInvoiceActivity.this).load(orderModel.getOrderProviderImg()).into(ivProvider);
            }
            //Customer dont need to know provider's address, so this will remain hidden.
            tvDeliveryAddress.setVisibility(View.GONE);
        }

    }
}
