package com.example.feedme.model;

import java.io.Serializable;

/**
 * Created by User on 2017-07-25.
 */

public class ItemModel implements Serializable {

    private String itemCreatorId;
    private String itemName, itemImage, itemId, invoideId;
    private double itemPrice;
    private boolean isTodaysSpecial;

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }


    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }


    public String getInvoideId() {
        return invoideId;
    }

    public void setInvoideId(String invoideId) {
        this.invoideId = invoideId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public boolean isTodaysSpecial() {
        return isTodaysSpecial;
    }

    public void setTodaysSpecial(boolean todaysSpecial) {
        isTodaysSpecial = todaysSpecial;
    }

    public String getItemCreatorId() {
        return itemCreatorId;
    }

    public void setItemCreatorId(String itemCreatorId) {
        this.itemCreatorId = itemCreatorId;
    }
}
