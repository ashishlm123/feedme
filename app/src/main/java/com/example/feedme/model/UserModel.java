package com.example.feedme.model;

import java.io.Serializable;

public class UserModel implements Serializable {
    private String userID;
    private String userName;
    private String userEmail;
    private String userMobile;
    private String userType;
    private String userAvailability;
    private String userFoodCertificateLink;
    private String userAddress;
    private String userImg;


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserAvailability() {
        return userAvailability;
    }

    public void setUserAvailability(String userAvailability) {
        this.userAvailability = userAvailability;
    }

    public String getUserFoodCertificateLink() {
        return userFoodCertificateLink;
    }

    public void setUserFoodCertificateLink(String userFoodCertificateLink) {
        this.userFoodCertificateLink = userFoodCertificateLink;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }
}
