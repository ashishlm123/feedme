package com.example.feedme.model;

import java.io.Serializable;

public class OrderModel implements Serializable {
    private String orderId;
    private String orderInvoiceId;
    private String orderDetails;
    private double orderTotalPrice;
    private String orderProviderName;
    private String orderProviderPhnNo;
    private String orderProviderAddress;
    private String paymentMethod;
    private String orderStatus;
    private String orderProviderImg;
    private String orderDate;

    public static String STATUS_DELIVERED = "delivered";
    public static String STATUS_PENDING = "pending";

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderInvoiceId() {
        return orderInvoiceId;
    }

    public void setOrderInvoiceId(String orderInvoiceId) {
        this.orderInvoiceId = orderInvoiceId;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }


    public String getOrderProviderName() {
        return orderProviderName;
    }

    public void setOrderProviderName(String orderProviderName) {
        this.orderProviderName = orderProviderName;
    }

    public double getOrderTotalPrice() {
        return orderTotalPrice;
    }

    public void setOrderTotalPrice(double orderTotalPrice) {
        this.orderTotalPrice = orderTotalPrice;
    }

    public String getOrderProviderAddress() {
        return orderProviderAddress;
    }

    public void setOrderProviderAddress(String orderProviderAddress) {
        this.orderProviderAddress = orderProviderAddress;
    }

    public String getOrderProviderPhnNo() {
        return orderProviderPhnNo;
    }

    public void setOrderProviderPhnNo(String orderProviderPhnNo) {
        this.orderProviderPhnNo = orderProviderPhnNo;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderProviderImg() {
        return orderProviderImg;
    }

    public void setOrderProviderImg(String orderProviderImg) {
        this.orderProviderImg = orderProviderImg;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }
}
