package com.example.feedme.helper;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {
    private static Api api;

    public static Api getInstance() {
        if (api != null) {
            return api;
        } else {
            api = new Api();
            return api;
        }
    }


    private String getHost() {
        return "https://feedme1.000webhostapp.com/";
    }

    public final String forgetPasswordApi = getHost() + "forgot_password.php";

    public final String loginApi = getHost() + "login.php";
    public final String registerApi = getHost() + "signup.php";
    public final String contactUsApi = getHost() + "contactus.php";

    //cart
    public final String addToCarApi = getHost() + "add_to_cart.php";
    public final String viewCartApi = getHost() + "cart_listing.php";
    public final String updateOrderApi = getHost() + "update_order.php";
    public final String removeCartItemAPI = getHost() + "delete_cart.php";


    public final String todaysSpecialApi = getHost() + "todays_special_listing.php";
    public final String getNearbyProvidersApi = getHost() + "provider_listing.php";
    public final String addnewItemApi = getHost() + "additems.php";
    public final String viewAvailableItemsApi = getHost() + "show_items_list.php";
    public final String itemDetailsApi = getHost() + "item_details.php";
    public final String deleteItemApi = getHost() + "delete_item.php";

    public final String placeOrderApi = getHost() + "place_order.php";
    public final String customerOrderListingApi = getHost() + "order_customer_listing.php";
    public final String providerOrderListingTask = getHost() + "order_provider_listing.php";


}